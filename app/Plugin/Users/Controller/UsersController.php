<?php
error_reporting(0);
App::uses('UsersAppController', 'Users.Controller');
App::import('Vendor', 'facebook/facebook');
/**
 * Users Controller
 *
 */
class UsersController extends UsersAppController {
	public $helpers = array('Crumb');

	/**
	 * variaveis do facebook
	 * @var array
	 */
	protected $offerFilters = array(
		'gender' => array(),
		'religion' => array(),
		'political' => array(),
		'location' => array(),		
		'age_group' => array(),
		'relationship_status' => array(),
	);
	
	/**
	 * Exibe lista de ofertas disponiveis de acordo com parametros
	 * DESEJOS, PERSONALIZADA, POR EMPRESA OU PUBLICAS
	 * @return void
	 */
	public function home() {
				
		//verifica se usuario esta vendo ofertas vindo de desejo 
		if($this->Session->check('ofertasIds')){
			//mostra oferta de desejos
			$data  = date('Y-m-d');		
			$limit = 5;
			$update = true;
							
			//total de ofertas
			$params 	 = array('Offer'=>array('conditions'=>array('Offer.status'=>'ACTIVE',  'Offer.id'=>$this->Session->read('ofertasIds')), 'order'=>array('Offer.id'=>'DESC')));																																	
			$contador	 = $this->Utility->urlRequestToGetData('offers', 'count', $params);						
			
			
			//verifica se esta fazendo uma requisicao ajax
			if(!empty($this->request->data['limit'])){			
				$render = true;
				$this->layout = '';																				
				$limit = $_POST['limit'] + 2;
				if($limit > $contador){
					$limit = $contador;		
					$update = false;	 	
				} 				
			}
			
			$params = array('Offer'=>array('conditions'=>array('Offer.status'=>'ACTIVE', 'Offer.id'=>$this->Session->read('ofertasIds')), 'order'=>array('Offer.id'=>'DESC'), 'limit'=>$limit));			
			$offers = $this->Utility->urlRequestToGetData('offers', 'all', $params);
			
		}else if($this->Session->read('HomeOfertas')=='personalizado'){									
			
			$data  = date('Y-m-d');		
			$limit = 5;
			$update = true;
			
			//verifica se usuario esta vendo oferta personalizada
			if($this->Session->check('Ofertas-Assinaturas')){					
				if($this->Session->read('Ofertas-Assinaturas.hdAllCompany')==1){				
					//ofertas de empresa					
					$params = array('Offer'=>array('conditions'=>array('Offer.company_id'=>$this->Session->read('Ofertas-Assinaturas.hdIdCOmpany'), 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data)));																																					
					$contador	 = $this->Utility->urlRequestToGetData('offers', 'count', $params);
					
				}else{
					$params = $this->ofertas_perfil($this->Session->read('Ofertas-Assinaturas.hdIdCOmpany'));		
					$contador = count($params);														
				}			
			}else{
				$params 	 = array('OffersUser'=>array('conditions'=>array('user_id'=>$this->Session->read('userData.User.id'), 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data), 'order'=>array('OffersUser.id'=>'DESC')), 'Offer');																																				
				$contador	 = $this->Utility->urlRequestToGetData('offers', 'count', $params);
			}				
			
			//verifica se esta fazendo uma requisicao ajax
			if(!empty($this->request->data['limit'])){			
				$render = true;
				$this->layout = '';																				
				$limit = $_POST['limit'] + 2;
				if($limit > $contador){
					$limit = $contador;		
					$update = false;	 	
				} 				
			}
			
			//verifica se usuario esta vendo oferta personalizada
			if($this->Session->check('Ofertas-Assinaturas')){	
				
				if($this->Session->read('Ofertas-Assinaturas.hdAllCompany')==1){				
					//ofertas de empresa
					$params = array('Offer'=>array('conditions'=>array('Offer.company_id'=>$this->Session->read('Ofertas-Assinaturas.hdIdCOmpany'), 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data), 'order'=>array('Offer.id'=>'DESC'), 'limit'=>$limit));
					$offers = $this->Utility->urlRequestToGetData('offers', 'all', $params);				
				}else{							
					$offers = $this->ofertas_perfil($this->Session->read('Ofertas-Assinaturas.hdIdCOmpany'), true, $limit);									
				}			
			}else{
				$params = array('OffersUser'=>array('conditions'=>array('user_id'=>$this->Session->read('userData.User.id'), 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data), 'order'=>array('OffersUser.offer_id'=>'DESC'), 'limit'=>$limit), 'Offer');
				$offers = $this->Utility->urlRequestToGetData('offers', 'all', $params);
			}
									
		}else{						
			//mostra oferta publica
			$data  = date('Y-m-d');		
			$limit = 5;
			$update = true;
							
			//total de ofertas
			$params 	 = array('Offer'=>array('conditions'=>array('public'=>'ACTIVE', 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data), 'order'=>array('Offer.id'=>'DESC')));																																	
			$contador	 = $this->Utility->urlRequestToGetData('offers', 'count', $params);						
			
			//verifica se esta fazendo uma requisicao ajax
			if(!empty($this->request->data['limit'])){			
				$render = true;
				$this->layout = '';																				
				$limit = $_POST['limit'] + 2;
				if($limit > $contador){
					$limit = $contador;		
					$update = false;	 	
				} 				
			}
			
			$params = array('Offer'=>array('conditions'=>array('public'=>'ACTIVE', 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data), 'order'=>array('Offer.id'=>'DESC'), 'limit'=>$limit));			
			$offers = $this->Utility->urlRequestToGetData('offers', 'all', $params);
		}
		
		//verifica quantidade de assinaturas			
		$params 		 = array('CompaniesUser'=>array('conditions'=>array('CompaniesUser.user_id'=>$this->Session->read('userData.User.id'), 'CompaniesUser.status'=>'ACTIVE')));
		$assinaturas	 = $this->Utility->urlRequestToGetData('companies', 'count', $params);		
		if($assinaturas<5)
			$assinaturas=true; 
		else 
			$assinaturas = false;
			
		//verifica se perfil esta completo
		$completarPerfil = $this->verificaPerfil();
		
		$this->set(compact('offers', 'limit', 'contador', 'update', 'assinaturas', 'completarPerfil'));
		if(!empty($render))$this->render('Elements/ajax_ofertas');
	}

	/**
	 * Exibe detalhes da oferta
	 * @return void
	 */
	public function viewOffer($offerId = null, $public = false) {		
		$this->Session->write('id', $offerId);		
		if($offerId == null){
			$this->redirect(array('controller' => 'users', 'action' => 'home', 'plugin' => 'users'));
		}else{
			//verifica se � requisicao ajax - trabalhando metricas
			if($this->request->is('ajax')) {				
				$this->Session->write('Carrinho.Opcoes.metricas.'.$this->request->data['chave'], $this->request->data['valor']);
				pr($_SESSION['Carrinho']);
				exit;			
			}				
				
			$params = array('Offer'=>array('conditions'=>array('Offer.id'=>$offerId)), 'OffersComment', 'OffersPhoto', 'Company');								
			$offer = $this->Utility->urlRequestToGetData('offers', 'first', $params);
			
			if(!empty($offer['OffersComment'])){
				$i = 0;
				foreach($offer['OffersComment'] as $comentario){
					$id_usuario = $comentario['user_id'];
					$params = array('User'=>array('fields'=>array('User.name'),'conditions'=>array('User.id'=>$id_usuario)));
					$user   = $this->Utility->urlRequestToGetData('users', 'first', $params);
					$offer['OffersComment'][$i]['nome'] = $user['User']['name'];
					$i++;										
				}
				
			}
			
			//calculando dias que estao faltando para finalizar oferta
			$dataInicio 			 = date('d/m/Y'); 
			$dataFinal  			 = date('d/m/Y', strtotime($offer['Offer']['ends_at']));			
			$dias 					 = $this->Utility->calcData($dataInicio, $dataFinal);
			$offer['Offer']['dias']  = $dias;		
		}	
		//verifica se � oferta publica	
		if($public==true){
			$this->Session->write('Carrinho.public', true);
		}
		$title_for_layout = $offer['Offer']['title'];	
		
		$this->set(compact('title_for_layout'));			
		$this->set('offers', $offer);		
		
		
	}	
	
	public function cad_perfil($id_oferta=null){
		$this->layout = 'login';

		$mensagem = null;
		if($this->request->is('post')){
						
			# valida campo em branco ou com placeholders			
			$placeholders = array('Nome', 'E-mail', 'Senha');
			foreach($this->request->data['User'] as $field) {
				if(empty($field) || in_array($field, $placeholders)){
					$this->set('mensagem', 'Dados inválidos. Tente novamente.'); 
					return;
				}
			}
			
			#valida campo email		
			$input = $this->request->data['User']['email'];			
			if (!$this->validaEmail($input)) {
				$this->set('mensagem', 'Email inválido. Tente novamente.'); 
				return;
			}
			
			
			$params = array('User'=>array('conditions'=>array('User.email'=>$this->request->data['User']['email'])));
			$verificaEmail = $this->Utility->urlRequestToGetData('users', 'count', $params);
			if($verificaEmail==true){
				$mensagem = "Esse email ja esta cadastrado no TRUEONE";
			}else{				
				$params = $this->request->data;
				
				$params['User']['date_register'] = date('Y/m/d');
				$params['User']['status'] = 'ACTIVE';
				unset($params['id_oferta']);			
					
				$user = $this->Utility->urlRequestToSaveData('users', $params);
										
				//cria sessao com dados do usuario				
				$this->Session->write('sessionLogado', true);
				$this->Session->write('userData', $user['data']);
				if($user){
					
					unset($params['User']['date_register']);					
					unset($params['User']['status']);					
					//pr($params);exit;
										
					//envia email de boas vindas
					$user_email = $this->Utility->urlRequestToLoginUserData('users','first',$params);
					
					if($this->Session->check('offerIDBeforePurchasing')) { // tentou compra antes de logar (levamos o cara de volta)
						$offerID = $this->Session->read('offerIDBeforePurchasing');
						$this->Session->delete('offerIDBeforePurchasing');
	
						$this->Session->write('Carrinho.AssinaEmpresa', true);
						$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'detail_purchasing', $offerID));
						
					} else {
						$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'home'));   
					}
				}else{
					$mensagem = "Ocorreu um erro no teu cadastro";					  
				}
								
				/*
				if(!empty($this->request->data['id_oferta'])){
					$this->Session->write('Carrinho.AssinaEmpresa', true);
					$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'detail_purchasing', $this->request->data['id_oferta']));
				}else{
					$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'home'));	
				}
				 */
			}								
		}
		$this->set(compact('mensagem', 'id_oferta'));
	}

	/**
	 * Seleciona endereco para entrega do produto
	 * @return void
	 */
	public function shippingAddress($cep = null) {						
		if($this->request->is('post') || $this->Session->check('Carrinho')){
			
			if(!empty($this->request->data['id_oferta'])){
				$params = array('Offer'=>array('conditions'=>array('Offer.id'=>$this->request->data['id_oferta'])), 'OffersComment', 'OffersPhoto');								
				$offer = $this->Utility->urlRequestToGetData('offers', 'first', $params);
				$this->Session->write('Carrinho.Oferta', $offer['Offer']);
							
			}
			
						
			//verifica se calcula cep (ajax)
			if(isset($this->request->data['cep'])){
														
				$render = true;
				$this->layout = '';
				$frete = $this->calculaFrete($this->request->data['cep']);
				if(!$frete==false){
					$frete 			= explode("-", $frete);			
					$frete[0] 		= str_replace(',', '.', $frete[0]);
					
					$this->Session->write('Carrinho.Oferta.value_frete',  $frete[0]);
					$this->Session->write('Carrinho.Oferta.prazo_entrega', $frete[1]);
								
					$valorFinal = $this->Session->read('Carrinho.Oferta.value_total') + $this->Session->read('Carrinho.Oferta.value_frete');
					$this->Session->write('Carrinho.Oferta.value_final', $valorFinal);	
				}else{
					$this->Session->write('Carrinho.Oferta.value_frete',  '');
					$this->Session->write('Carrinho.Oferta.prazo_entrega', '');												
					$this->Session->write('Carrinho.Oferta.value_final', '');
				}																						
			}else{			
				if(!empty($this->request->data)){
					
					if($this->request->data['quantidade']=='quantidade'){
						$this->request->data['quantidade'] = 1;	
					}																	
				}
										
				$this->Session->write('Carrinho.Opcoes', $this->request->data);	
			}										
		}
		if(!empty($render))$this->render('Elements/ajax_cep');
	}

	/**
	 * Seleciona endereco para entrega do produto
	 * @return void
	 */
	public function purchaseDetails() {
		$erro = false;
		
		if($this->request->is('post')|| $this->Session->check('Carrinho')){
			
			//numero do pedido
			$params   = array('Checkout'=>array('order'=>array('Checkout.id'=>'DESC')));
			$checkout = $this->Utility->urlRequestToGetData('payments', 'first', $params);
			$ultimaCompra = $checkout['Checkout']['id'] + 1;
			
			//verifica se � requisao ajax
			if(isset($this->request->data['quantidade'])){				
				$this->Session->write('Carrinho.Opcoes.quantidade',  $this->request->data['quantidade']);
				$render = true;
				$this->layout = '';
				
			}else{
				$this->Session->write('Carrinho.Endereco', $this->request->data);	
			}					
			if(!$this->Session->write('Carrinho.Oferta.value_frete')==''){
				$frete = $this->calculaFrete($this->Session->read('Carrinho.Endereco.cep'));
																							
				if($frete==false){					
					$erro = true;															
				}else{				 			
					$frete 			= explode("-", $frete);			
					$frete[0] 		= str_replace(',', '.', $frete[0]);
					
					$this->Session->write('Carrinho.Oferta.value_frete',  $frete[0]);
					$this->Session->write('Carrinho.Oferta.prazo_entrega', $frete[1]);
								
					$valorFinal = $this->Session->read('Carrinho.Oferta.value_total') + $this->Session->read('Carrinho.Oferta.value_frete');
					$this->Session->write('Carrinho.Oferta.value_final', $valorFinal);
				}	
			}						
		}else{
			$this->redirect(array('controller' => 'users', 'action' => 'home', 'plugin' => 'users'));
		}
		$this->set(compact('erro', 'ultimaCompra'));
		if(!empty($render))$this->render('Elements/ajax_quantidade');					
	}

	/**
	 * Pagamento da compra realizada
	 * @return void
	 */
	public function checkout_() {		
		if($this->Session->read('Carrinho.Oferta.value_frete')=='' || $this->Session->read('Carrinho.Opcoes.quantidade')==''){
			$this->redirect(array('controller' => 'users', 'action' => 'purchaseDetails', 'plugin' => 'users'));	
		}
		if($this->Session->check('Carrinho')){
				
			//verifica se oferta � publica			
			if($this->Session->read('Carrinho.Oferta.public')=='ACTIVE'){										
				$params = array('CompaniesUser'=>array('conditions'=>array('CompaniesUser.company_id'=>$this->Session->read('Carrinho.Oferta.company_id'), 'CompaniesUser.user_id'=>$this->Session->read('userData.User.id'))));
				$countSignatures = $this->Utility->urlRequestToGetData('companies', 'count', $params);
				
				//verifica se usuario ja esta assinado a empresa
				if(!$countSignatures==true){					
					$params = array('CompaniesUser'=>array('company_id'=>$this->Session->read('Carrinho.Oferta.company_id'), 'user_id'=>$this->Session->read('userData.User.id'), 'status'=>'ACTIVE', 'last_status'=>'ACTIVE', 'date_register'=>date('Y/m/d')));
					//assina usuario a empresa que fez oferta publica									
					$signature = $this->Utility->urlRequestToSaveData('companies', $params);									
				}			
			}
			
			//criando array para insert de pagamentos							
			$arrayParams = array('Checkout'=>
									array(
										  'Checkout.user_id'=> $this->Session->read('userData.User.id'),
										  'Checkout.company_id'=> $this->Session->read('Carrinho.Oferta.company_id'),
										  'Checkout.payment_method_id'=>3,
										  'Checkout.offer_id'=> $this->Session->read('Carrinho.Oferta.id'),
									      'Checkout.payment_state_id'=>14,
										  'Checkout.unit_value'=> $this->Session->read('Carrinho.Oferta.value'),
									      'Checkout.total_value'=> $this->Session->read('Carrinho.Oferta.value_total'),
									      'Checkout.amount'=> $this->Session->read('Carrinho.Opcoes.quantidade'),
									      'Checkout.shipping_value'=> $this->Session->read('Carrinho.Oferta.value_frete'),
									      'Checkout.shipping_type'=> 'CORREIOS',
									      'Checkout.delivery_time'=> $this->Session->read('Carrinho.Oferta.prazo_entrega'),
										  'Checkout.metrics'=> 'teste',	
									      'Checkout.address'=> $this->Session->read('Carrinho.Endereco.endereco'),
									      'Checkout.city'=> $this->Session->read('Carrinho.Endereco.cidade'),
										  'Checkout.zip_code'=> $this->Session->read('Carrinho.Endereco.cep'),
										  'Checkout.state'=> $this->Session->read('Carrinho.Endereco.uf'),
										  'Checkout.district'=> $this->Session->read('Carrinho.Endereco.bairro'),
										  'Checkout.number'=> $this->Session->read('Carrinho.Endereco.numero'),
										  'Checkout.date'=> date('Y-m/d')											
									)
								);			
			$pagamento = $this->Utility->urlRequestToSaveData('payments', $arrayParams);		
			
						
			//chamada do moip 
			$arrayParams = array('Payments'=>array(
								'parcelamento_juros' => 'ACTIVE',
								'key'=>'SKMQ5HKQFTFRIFQBJEOROIGM70I6QVIN9KA5YIWB', 
								'token'=>'WOA4NBQ2AUMHJQ2NJIA6Q6X4ECXHFJUR', 
							   	'idUnique'=>$pagamento['Checkout']['id'], 
							   	'reason'=>$this->Session->read('Carrinho.Oferta.title'),
							   	'value'=>$this->Session->read('Carrinho.Oferta.value_final'),										   
							   	'setPlayer'=>array(								 			
							   		'name'=>$this->Session->read('userData.User.name'),
									'email'=>$this->Session->read('userData.User.email'),
									'payerId'=>$this->Session->read('userData.User.id'),												 
							   		'billingAddress'=>array(
										'address'=>$this->Session->read('Carrinho.Endereco.endereco'), 
										'number'=>$this->Session->read('Carrinho.Endereco.numero'),
										'complement' => '',
							            'city' => $this->Session->read('Carrinho.Endereco.cidade'),
							            'neighborhood' => $this->Session->read('Carrinho.Endereco.bairro'),
							            'state' => $this->Session->read('Carrinho.Endereco.uf'),
							            'country' => 'BRA',
							            'zipCode' => $this->Session->read('Carrinho.Endereco.cep'),
							            'phone' => '(11)9950-0989'
										)
									)
								)
							);
							
			$token = $this->Utility->urlRequestToCheckout('payments', $arrayParams);					
			$this->set('token', $token);
			
		}	
			
	}
	
	/**
	 * Compras realizadas pelo usuario
	 * Insert de comentarios
	 * @return void
	 */
	public function minhasCompras(){
		$update = true;
		
		//verifica se � requisicao ajax
		if(!empty($this->request->data['id_oferta'])) {
					
			
				
			//recebe linha do comentario para update e id da oferta
			$idComentario 	 = $this->request->data['id_comentario'];
			$idOferta		 = $this->request->data['id_oferta'];									
			
			if($idComentario==0){
				$arrayParams = array(						
	 			'OffersComment' => array(
	 				'offer_id'=> $idOferta,
					'user_id'=> $this->Session->read('userData.User.id'),
					'title'=> $this->request->data['opiniao'],
					'description'=> $this->request->data['textarea'],
					'evaluation'=> '-',
					'date_register'=> date('Y/m/d'),
					'status'=>'INACTIVE'										 
		 			),
		 		);	
			}else{				
				$arrayParams = array(						
	 			'OffersComment' => array(
	 				'id' => $idComentario,				
					'title'=> $this->request->data['opiniao'],
					'description'=> $this->request->data['textarea'],
					'evaluation'=> '-'				 	 						
		 			),
		 		);
			}			
			$updateStatus = $this->Utility->urlRequestToSaveData('offers', $arrayParams);
			
		}
		
						
		//contador
		$limit = 3;
		$params     = array('Checkout'=>array('conditions'=>array('Checkout.user_id'=>$this->Session->read('userData.User.id'), 'NOT'=>array('Checkout.payment_state_id'=>array('999', '14')))));
		$contador   = $this->Utility->urlRequestToGetData('payments', 'count', $params);		
		
		//verifica se esta fazendo uma requisicao ajax
		if(!empty($this->request->data['limit'])){			
			$render = true;
			$this->layout = '';																				
			$limit = $_POST['limit'] + 4;
			if($limit > $contador){
				$limit = $contador;
				
				//nao chama mais atualizacao	
				$update = false;		 	
			} 				
		}
		
		$params     = array('Checkout'=>array('conditions'=>array('Checkout.user_id'=>$this->Session->read('userData.User.id'), 'NOT'=>array('Checkout.payment_state_id'=>array('999', '14'))), 'order'=>array('Checkout.id'=>'DESC'), 'limit'=>$limit), 'Offer', 'PaymentState', 'PaymentMethod', 'Company');			
		$checkouts  = $this->Utility->urlRequestToGetData('payments', 'all', $params);
		
		if(is_array($checkouts)){
			foreach($checkouts as $checkout){									
				
				//verificando se usuario ja comentou a oferta
				$idOferta 	= $checkout['Checkout']['offer_id'];
				$idEmpresa	= $checkout['Checkout']['company_id'];
				
			    $params		= array('OffersComment'=>array('conditions'=>array('OffersComment.offer_id'=>$idOferta, 'OffersComment.user_id'=>$this->Session->read('userData.User.id'))));			
				$comentario	= $this->Utility->urlRequestToGetData('offers', 'first', $params);

				$params		= array('Company'=>array('conditions'=>array('Company.id'=>$idEmpresa)));			
				$empresa	= $this->Utility->urlRequestToGetData('companies', 'first', $params);
				
				if(!$comentario==true)$comentario=false;			
				$checkout['Checkout']['comment'] = $comentario;
				$checkout['Company'] = $empresa['Company'];				
				$checks[] = $checkout;			 								
			}	
		}else{
			$checks = "NENHUMA COMPRA OU PEDIDO ENCONTRADO";
		}
			
		
		$this->set(compact('checks', 'limit', 'contador', 'update'));	
		if(!empty($render))$this->render('Elements/ajax_compras');
	}
	
	
	/**
	 * Assinaturas realizadas pelo usuario	
	 * @return void
	 */
	public function signatures(){
							
		//total de empresas
		$params 	 = array('CompaniesUser'=>array('conditions'=>array('user_id'=>$this->Session->read('userData.User.id'), 'Company.status'=>'ACTIVE', 'CompaniesUser.status'=>'ACTIVE'), 'order'=>array('CompaniesUser.id'=>'DESC')), 'Company');																																	
		$contador	 = $this->Utility->urlRequestToGetData('companies', 'count', $params);						
			
					
		//verifica se � mudanca de status
		if(!empty($this->request->data['update'])=='true'){
								
			//update - linha existe
			if($this->request->data['up']==0){
				$arrayParams = array(						
	 			'CompaniesUser' => array(
	 				'id' => $this->request->data['id'],				
					'status'=>$this->request->data['status']				 	 						
		 			),
		 		);		
		 	//insert - linha nao existe
			}else{
				$arrayParams = array(						
	 			'CompaniesUser' => array(
	 				'user_id'=>  $this->Session->read('userData.User.id'),
					'company_id'=> $this->request->data['id'],
					'status'=> 'ACTIVE',
					'date_register'=> date('Y/m/d'),
					'status'=>'ACTIVE'										 
		 			),
		 		);
		 		$data = date('Y/m/d');		 				 				
				$offersCompany = $this->ofertas_perfil($this->request->data['id'], null, null, true);					
				foreach($offersCompany as $offerSignature){
					$offerId 		= $offerSignature['Offer']['id'];					
					$params 	= array('OffersUser'=>array('conditions'=>array('OffersUser.user_id'=>$this->Session->read('userData.User.id'), 'offer_id'=>$offerId)));
					$oferta     = $this->Utility->urlRequestToGetData('users', 'count', $params);
								
					if(!$oferta>0){	
						$query		 	= "INSERT INTO offers_users values(NULL, '{$offerId}', '{$this->Session->read('userData.User.id')}', '{$data}', 'facebook - portal')";				
						$params		    = array('User' => array('query'=>$query));					
						$addUserOffer = $this->Utility->urlRequestToGetData('users', 'query', $params);
					}
				}	
			}
								
			$updateStatus = $this->Utility->urlRequestToSaveData('companies', $arrayParams);
			//pr($updateStatus);exit;								
		}
		//verifica se esta fazendo uma requisicao ajax para pesquisa
		if(!empty($this->request->data['ref'])=='true'){			
			$render = true;
			$this->layout = '';	
			$busca 		 = $this->request->data['pesquisa'];
			$params 	 = array('Company'=>array('conditions'=>array('Company.status'=>'ACTIVE', 'Company.fancy_name LIKE'=>"%{$busca}%"), 'order'=>array('Company.id'=>'DESC')));					
			$companiesA   = $this->Utility->urlRequestToGetData('companies', 'all', $params);
			if($companiesA){																						
				foreach($companiesA as $company){				
					$params 	 	 = array('CompaniesUser'=>array('conditions'=>array('user_id'=>$this->Session->read('userData.User.id'), 'company_id'=>$company['Company']['id'], 'Company.status'=>'ACTIVE'), 'order'=>array('CompaniesUser.id'=>'DESC')), 'Company');			
					$companiesUser   = $this->Utility->urlRequestToGetData('companies', 'first', $params);
					if($companiesUser){
						$companies[] = $companiesUser; 
					}else{
						$companies[] = $company;
					}
				}
			}else{
				$companies = "NENHUMA EMPRESA ENCONTRADA";	
			}											
		}else{
			$data = date('Y/m/d');
			$params 	 	= array('CompaniesUser'=>array('conditions'=>array('user_id'=>$this->Session->read('userData.User.id'), 'Company.status'=>'ACTIVE', 'CompaniesUser.status'=>'ACTIVE'), 'order'=>array('CompaniesUser.id'=>'DESC')), 'Company');			
			$companiesUser   = $this->Utility->urlRequestToGetData('companies', 'all', $params);
			
			//pegando quantidade de ofertas de empresa total e de acordo com perfil
			foreach($companiesUser as $company){		
				//quantidade de ofertas de empresa
				$params 		= array('Offer'=>array('conditions'=>array('Offer.company_id'=>$company['Company']['id'], 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data)));
				$ofertas     	= $this->Utility->urlRequestToGetData('offers', 'count', $params);
				$company['Company']['ofertas'] = $ofertas;
				
				//pegando ofertas de acordo com perfil
				$ofertas_perfil = $this->ofertas_perfil($company['Company']['id']);
				$quantidade_perfil = count($ofertas_perfil);
				$company['Company']['ofertas_perfil'] = $quantidade_perfil;
				$companies[] = $company;							
			}									
		}
											
		$this->set(compact('companies', 'limit', 'contador'));
		if(!empty($render))$this->render('Elements/ajax_assinaturas');
	}
	
	
	/**
	 * Efetua login de usuario no portal
	 * @return void
	 */
	public function login(){		
		$this->layout = 'login';
		
		$facebook = new Facebook(
			array(
			 	 'appId'  => '447466838647107',
			  	 'secret' => 'b432e357dd19491aa8d45acd7074b2f6',
			)
		 );	
		
		// obtem o id do usuario
		$user = $facebook->getUser();	
		
		if ($user) { // usuario logado
	        try {
	        // Obtem dados do usuario logado
	        $user_profile = $facebook->api('/me');
			$photo = "https://graph.facebook.com/$user/picture";
			//pr($photo);exit;	        		        
	        //verifica se usuario ja existe no trueone
	       	$params 			= array('FacebookProfile'=>array('conditions'=>array('FacebookProfile.facebook_id'=>$user)),'User');
	       	$facebookProfile    = $this->Utility->urlRequestToGetData('users', 'first', $params);
	       
		       	if(is_array($facebookProfile)){
		       		//verifica se precisa fazer atualizacao na tabela facebook_profiles		       		
		       		if($facebookProfile['FacebookProfile']['updated_time'] < date('Y/m/d h:i:s', strtotime($user_profile['updated_time']))){
		       			if(empty($user_profile['relationship_status']))$user_profile['relationship_status'] = '';
			       		if(empty($user_profile['religion']))$user_profile['religion'] = '';
			       		if(empty($user_profile['political']))$user_profile['political'] = '';
			       		if(empty($user_profile['updated_time']))$user_profile['updated_time'] = '';
			       		if(empty($user_profile['location']))$user_profile['location']['name'] = '';
		       			$params = array('User'=>array('id'=>$facebookProfile['FacebookProfile']['user_id']),
		       							'FacebookProfile'=>array(
		       							'id'=>$facebookProfile['FacebookProfile']['id'],
		       							'facebook_id'=>$user,
		       							'user_id'=>$facebookProfile['FacebookProfile']['user_id'],
		       							'first_name'=>$user_profile['name'],
		       							'last_name'=>$user_profile['last_name'],
		       							'email'=>$user_profile['email'],
		       							'gender'=>$user_profile['gender'],
		       							'profile_link'=>$user_profile['link'],
		       							'birthday'=> date('Y/m/d', strtotime($user_profile['birthday'])),
		       							'location'=>$user_profile['location']['name'],
		       							'relationship_status'=>$user_profile['relationship_status'],
		       							'religion'=>$user_profile['religion'],
		       							'political'=>$user_profile['political'],
		       							'updated_time'=>$user_profile['updated_time']	       												
		       					)
		       				);	
		       			
		       			$updateUsuario = $this->Utility->urlRequestToSaveData('users', $params);
		       			
		       			//cria sessao com dados da empresa				
						$this->Session->write('sessionLogado', true);						
						$this->Session->write('userData', $updateUsuario['data']);																										
						
						if($this->Session->check('offerIDBeforePurchasing')) { // tentou compra antes de logar (levamos o cara de volta)
							$offerID = $this->Session->read('offerIDBeforePurchasing');
							$this->Session->delete('offerIDBeforePurchasing');

							$this->Session->write('Carrinho.AssinaEmpresa', true);
							$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'detail_purchasing', $offerID));
							
						} else {
							$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'home'));   
						}

		       		}else{
		       			
		       			//cria sessao com dados da empresa				
						$this->Session->write('sessionLogado', true);
						$this->Session->write('userData', $facebookProfile);																										
						
						if($this->Session->check('offerIDBeforePurchasing')) { // tentou compra antes de logar (levamos o cara de volta)
							$offerID = $this->Session->read('offerIDBeforePurchasing');
							$this->Session->delete('offerIDBeforePurchasing');

							$this->Session->write('Carrinho.AssinaEmpresa', true);
							$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'detail_purchasing', $offerID));
							
						} else {
							$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'home'));   
						}

		       		}		       				       	
		       	}else{	    		       		  
		       		if(empty($user_profile['relationship_status']))$user_profile['relationship_status'] = '';
		       		if(empty($user_profile['religion']))$user_profile['religion'] = '';
		       		if(empty($user_profile['political']))$user_profile['political'] = '';
		       		if(empty($user_profile['updated_time']))$user_profile['updated_time'] = '';
		       		if(empty($user_profile['location']))$user_profile['location']['name'] = '';		 
		       		      		 				       	
		       		$params = array(
		       						'User'=>array(
			       						'name'=> $user_profile['name'],
		       							'password'=> $user_profile['email'],
			       						'email'=> $user_profile['email'],
			       						'gender'=> $user_profile['gender'],
			       						'birthday'=> date('Y/m/d', strtotime($user_profile['birthday'])),
			       						'photo'=> $photo,	
			       						'status'=>'ACTIVE',
			       						'date_register'=>date('Y/m/d')		
	       						),
		       						'FacebookProfile'=>array(
		       							'facebook_id'=>$user,
		       							'name'=>$user_profile['name'],
		       							'first_name'=>$user_profile['first_name'],
		       							'last_name'=>$user_profile['last_name'],
		       							'email'=>$user_profile['email'],
		       							'gender'=>$user_profile['gender'],
		       							'profile_link'=>$user_profile['link'],
		       							'birthday'=> date('Y/m/d', strtotime($user_profile['birthday'])),
		       							'location'=>$user_profile['location']['name'],
		       							'relationship_status'=>$user_profile['relationship_status'],
		       							'religion'=>$user_profile['religion'],
		       							'political'=>$user_profile['political'],
		       							'updated_time'=>$user_profile['updated_time']	       												
	       						)
		       				);		 
		       		
		       		$insertUsuario = $this->Utility->urlRequestToSaveData('users', $params);
		       			       		
		       		//cria sessao com dados da empresa				
					$this->Session->write('sessionLogado', true);
					$this->Session->write('userData', $insertUsuario['data']);

					if($this->Session->check('offerIDBeforePurchasing')) { // tentou compra antes de logar (levamos o cara de volta)
						$offerID = $this->Session->read('offerIDBeforePurchasing');
						$this->Session->delete('offerIDBeforePurchasing');

						$this->Session->write('Carrinho.AssinaEmpresa', true);
						$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'detail_purchasing', $offerID));
						
					} else {
						$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'home'));   
					}																						
					    		
		       	}	       			
	        } 
	        catch (FacebookApiException $e) {
	                error_log($e);
	                $user = null;
	        }	                      
		}else{
			$params = array(
		  		'scope' => 'email, user_about_me, user_birthday, user_hometown, user_location, user_relationships, user_religion_politics'		
			 );		
			$loginUrl = $facebook->getLoginUrl($params);			
		}
		
		if($this->request->is('post')){			
			if(!empty($this->request->data['Company']['emailPass'])){
			
				$array['User']['status'] = 'ACTIVE';
				$array['User']['email'] = $this->request->data['User']['emailPass'];				
				$resultado = $this->Utility->urlRequestToGetData('users', 'first', $array);									
								
				if($resultado){					
					$senha = time();
					$arraySenha = array('User'=>array('id'=>$resultado['User']['id'], 'password'=>$senha));					
																								
					$save = $this->Utility->urlRequestToSaveData('users', $arraySenha);
					$mensagem = "Sua nova senha {$senha}";
					
					/*
					 * TESTAR TESTAR TESTAR TESTAR 
					 * TESTAR TESTAR TESTAR TESTAR
					 */
					$this->Email->from    = 'wilanetonet@ig.com.br';
					$this->Email->to      = 'wca_junior2@ig.com.br';
					$this->Email->subject = 'Nova senha do teu cadastro';
					$this->Email->send($mensagem);
					
				}
				
			}else{	
												
				$array = $this->request->data['User'];
								
				//tratando array para fazer login corretamente com condicoes							
				$array['status'] 	 = 'ACTIVE';				
				$array['password'] 	 = $this->securityForm($array['password']);
				$array['email'] 	 = $this->securityForm($array['email']);					
				$array['password']	 = md5($array['password']);
				
				//arruma ARRAY de acordo com FIND do cakephp
				$array = array('User'=>array('conditions'=>$array));																																		
				$resultado = $this->Utility->urlRequestToGetData('users', 'first', $array);					
								
				if(is_array($resultado)){					
					if(!empty($resultado['User']['password'])){						
						if($resultado['User']['password'] == md5($this->request->data['User']['password'])){													
													
							//cria sessao com dados da empresa				
							$this->Session->write('sessionLogado', true);
							$this->Session->write('userData', $resultado);

							if($this->Session->check('offerIDBeforePurchasing')) { // tentou compra antes de logar (levamos o cara de volta)
								$offerID = $this->Session->read('offerIDBeforePurchasing');
								$this->Session->delete('offerIDBeforePurchasing');

								$this->Session->write('Carrinho.AssinaEmpresa', true);
								$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'detail_purchasing', $offerID));
								
							} else {
								$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'home'));   
							}

													
						}else{
							$this->Session->setFlash('Dados inválidos. Tente novamente.');				
						}	
					}else{
						$this->Session->setFlash('Dados inválidos. Tente novamente.');
					}			
				}else{
					$this->Session->setFlash('Dados inválidos. Tente novamente.');
				}
			}						
		}
		
		$this->set(compact('loginUrl'));
	}
	
	
	
	public function logoof(){
		session_destroy();
		$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'login'));	
	}
	
	
	public function detail_purchasing($id=null){		
		if(!$this->Session->check('userData.User')){
			//$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'cad_perfil', $this->request->data['id_oferta']));

			# redirecionar para login, n�o para cadastro de perfil.
			$this->Session->write('offerIDBeforePurchasing', $this->request->data['id_oferta']);
			$this->redirect(array('controller'=>'users', 'plugin'=>'users', 'action'=>'login'));
		}
		if(!$this->Session->check('access')){	
			if(!empty($id)){			
				$this->request->data['id_oferta'] = $id;
				$this->Session->write('access', true);
			}	
		}
		//session_destroy();exit;				
		$one   = false;
		$frete = false;
		$erro  = false;
		$render = false;
		$token = 0;
		
		//verifica se requisicao � valida
		if($this->Session->check('Carrinho') || !empty($this->request->data['id_oferta'])){
			
			//verifica se � requisicao ajax
			if($this->request->is('ajax')) {
				
				$render = true;
				$this->layout = '';
				
				//ajax selecionando endereco
				if(!empty($this->request->data['excluir_endereco'])){	
					$query = "DELETE FROM aditional_addresses_users WHERE id = '{$this->request->data['id_endereco']}'";										
            		$params = array('User' => array('query' => $query));
            		$delEnd = $this->Utility->urlRequestToGetData('users', 'query', $params);            																
				}				
				//ajax selecionando endereco
				if(!empty($this->request->data['seleciona_endereco'])){					
					$params = array('AditionalAddressesUser'=>array('conditions'=>array('id'=>$this->request->data['id_endereco'])));
					$endereco = $this->Utility->urlRequestToGetData('users', 'first', $params);
					
					$this->Session->write('Carrinho.Endereco.id', $endereco['AditionalAddressesUser']['id']);
					$this->Session->write('Carrinho.Endereco.endereco', $endereco['AditionalAddressesUser']['address']);
					$this->Session->write('Carrinho.Endereco.cidade', $endereco['AditionalAddressesUser']['city']);
					$this->Session->write('Carrinho.Endereco.cep', $endereco['AditionalAddressesUser']['zip_code']);
					$this->Session->write('Carrinho.Endereco.estado', $endereco['AditionalAddressesUser']['state']);
					$this->Session->write('Carrinho.Endereco.bairro', $endereco['AditionalAddressesUser']['district']);
					$this->Session->write('Carrinho.Endereco.complemento', $endereco['AditionalAddressesUser']['complement']);
					$this->Session->write('Carrinho.Endereco.numero', $endereco['AditionalAddressesUser']['number']);
					$this->Session->write('Carrinho.Endereco.descricao', $endereco['AditionalAddressesUser']['label']);					
				}				
				//ajax add enderecos
				if(!empty($this->request->data['add_endereco'])){
					
					//verifica se usuario ja tem endereco cadastrado no perfil
					$params = array('User'=>array('conditions'=>array('User.id'=>$this->Session->read('userData.User.id'))));
					$verificaEndUser = $this->Utility->urlRequestToGetData('users', 'first', $params);
										
					if(empty($verificaEndUser['User']['zip_code'])){
						$params = array('User'=>array('id'=>$this->Session->read('userData.User.id'),'zip_code'=>$this->Session->read('Carrinho.Endereco.cep'), 'address'=>$this->Session->read('Carrinho.Endereco.endereco'), 'complement'=>$this->Session->read('Carrinho.Endereco.complemento'), 'district'=>$this->Session->read('Carrinho.Endereco.bairro'), 'city'=>$this->Session->read('Carrinho.Endereco.cidade'), 'state'=>$this->Session->read('Carrinho.Endereco.estado'), 'number'=>$this->Session->read('Carrinho.Endereco.numero')));
						//executa acao de insert ou update															
						$endUser = $this->Utility->urlRequestToSaveData('users', $params);						
						
					}
					//verificando se endereco ja existe
					$params = array('AditionalAddressesUser'=>array('conditions'=>array('zip_code'=>$this->Session->read('Carrinho.Endereco.cep'), 'number'=>$this->Session->read('Carrinho.Endereco.numero'), 'complement'=>$this->Session->read('Carrinho.Endereco.complemento')), 'fields'=>array('id')));
					$verEnd = $this->Utility->urlRequestToGetData('users', 'first', $params);
					
					//verifica se vai editar ou fazer insert de endereco
					if(is_array($verEnd)){						
						$params = array('AditionalAddressesUser'=>
									array('id'=>$verEnd['AditionalAddressesUser']['id'],
										  'user_id'=>$this->Session->read('userData.User.id'),
										  'label'=>$this->Session->read('Carrinho.Endereco.descricao'),
										  'address'=>$this->Session->read('Carrinho.Endereco.endereco'),										  
										  'complement'=>$this->Session->read('Carrinho.Endereco.complemento'),
										  'district'=>$this->Session->read('Carrinho.Endereco.bairro'),
									 	  'city'=>$this->Session->read('Carrinho.Endereco.cidade'),
									      'state'=>$this->Session->read('Carrinho.Endereco.estado')									      
									)
								);
					}else{						
						$query = "INSERT INTO aditional_addresses_users 
												VALUES(NULL, 
													  '{$this->Session->read('userData.User.id')}', 
													  '{$this->Session->read('Carrinho.Endereco.descricao')}', 
													  '{$this->Session->read('Carrinho.Endereco.endereco')}', 
													  '{$this->Session->read('Carrinho.Endereco.numero')}', 
													  '{$this->Session->read('Carrinho.Endereco.complemento')}', 
													  '{$this->Session->read('Carrinho.Endereco.bairro')}',
													  '{$this->Session->read('Carrinho.Endereco.cidade')}',
													  '{$this->Session->read('Carrinho.Endereco.estado')}',
													  '{$this->Session->read('Carrinho.Endereco.cep')}')";  											
										
			            $params = array('User' => array('query' => $query));			           			            
					}
					//executa acao de insert ou update					
					$ends = $this->Utility->urlRequestToGetData('users', 'query', $params);
																												
				}				
				//ajax metricas
				if(!empty($this->request->data['chave'])){						
					//$this->Session->write("Carrinho.Opcoes.metricas.{$this->request->data['chave']}",  $this->request->data['valor']);
					if(isset($_SESSION['Carrinho']['Opcoes']['metricas'][$this->request->data['chave']])){
						$_SESSION['Carrinho']['Opcoes']['metricas'][$this->request->data['chave']] = $this->request->data['valor'];
					}
					exit;																								
				}
				//ajax quantidade
				if(!empty($this->request->data['quantidade'])){												
					$this->Session->write('Carrinho.Opcoes.quantidade',  $this->request->data['quantidade']);					
					$frete = $this->calculaFrete($this->Session->read('Carrinho.Endereco.cep'));					
				}
				//ajax numero
				if(!empty($this->request->data['numero'])){				
					$this->Session->write('Carrinho.Endereco.numero', $this->request->data['numero']);
					exit;				
				}
				//ajax endereco
				if(!empty($this->request->data['endereco'])){				
					$this->Session->write('Carrinho.Endereco.endereco', $this->request->data['endereco']);
					exit;				
				}
				//ajax bairro
				if(!empty($this->request->data['bairro'])){				
					$this->Session->write('Carrinho.Endereco.bairro', $this->request->data['bairro']);
					exit;				
				}
				//ajax cidade
				if(!empty($this->request->data['cidade'])){				
					$this->Session->write('Carrinho.Endereco.cidade', $this->request->data['cidade']);
					exit;				
				}
				//ajax estado
				if(!empty($this->request->data['estado'])){				
					$this->Session->write('Carrinho.Endereco.estado', $this->request->data['estado']);
					exit;				
				}
				//ajax descricao
				if(!empty($this->request->data['descricao'])){				
					$this->Session->write('Carrinho.Endereco.descricao', $this->request->data['descricao']);
					exit;				
				}
				//ajax complemento
				if(!empty($this->request->data['complemento'])){				
					$this->Session->write('Carrinho.Endereco.complemento', $this->request->data['complemento']);
					exit;				
				}
				//ajax para definir tipo de pagamento da compra realizada
				if(!empty($this->request->data['tipo_pagamento'])){				
					$this->Checkout(false, false, true);
					exit;				
				}
				//ajax cep
				if(!empty($this->request->data['cep'])){	
					$cep = str_replace('-', '', $this->request->data['cep']);																		
					$frete = $this->calculaFrete($this->request->data['cep']);
					
					if(!$frete==false){										
						$cURL = curl_init("http://cep.correiocontrol.com.br/{$cep}.json");						
						curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);						
						$resultado = curl_exec($cURL);						
						curl_close($cURL);
						
						//pegando endereco
						$endereco = json_decode($resultado, true);		
						if(is_array($endereco)){
							$this->Session->write('Carrinho.Endereco.endereco', $endereco['logradouro']);
							$this->Session->write('Carrinho.Endereco.cidade', $endereco['localidade']);
							$this->Session->write('Carrinho.Endereco.cep', $endereco['cep']);
							$this->Session->write('Carrinho.Endereco.estado', $endereco['uf']);
							$this->Session->write('Carrinho.Endereco.bairro', $endereco['bairro']);
							$this->Session->write('Carrinho.Endereco.complemento', '');
							$this->Session->write('Carrinho.Endereco.numero', '');	
						}else{
							$erro = true;	
						}																
					}else{
						
					}						
				}				
				//ajax finalizando compra e retornando token
				if(!empty($this->request->data['compra'])){									
					$render = false;
					$renderToken = true;					
					$token = $this->Checkout(false, true);
					// vepr($token);exit;
					echo $token['token'];exit;										
					//$this->set('token', $token['token']);
					//echo "<input type=\"button\" id=\"boleto\" class=\"btn\" value=\"Clique para gerar o boleto bancário\" onClick=\"window.open('https://desenvolvedor.moip.com.br/sandbox/Instrucao.do?token={$token['token']}')\">";
					//if(!empty($render))$this->render('Elements/ajax_token');
					//exit;								
				}
			
			}else{
			
				//verifica se � a primeira vez que esta sendo acessada para criacao de sessao carrinho
				if(!empty($this->request->data['id_oferta'])){
					
					//primeiro acesso
					$one = true;
					$params = array('Offer'=>array('conditions'=>array('Offer.id'=>$this->request->data['id_oferta'])), 'OffersComment', 'OffersPhoto');								
					$offer = $this->Utility->urlRequestToGetData('offers', 'first', $params);
					$this->Session->write('Carrinho.Oferta', $offer['Offer']);
					if($offer['Offer']['percentage_discount'] > 0){
						$valor = ($offer['Offer']['value'] * (100 - $offer['Offer']['percentage_discount'])) / 100;
						$this->Session->write('Carrinho.Oferta.value', $valor);	
					}					
					
					$this->Session->write('Carrinho.Opcoes.quantidade', '1');					
					$total 	= $this->Session->read('Carrinho.Opcoes.quantidade') * $this->Session->read('Carrinho.Oferta.value');
					//$total  = number_format($total,2,',','.');
					$this->Session->write('Carrinho.Oferta.value_total', $total);
																			
					
					//trabalhando metricas
					if(!empty($offer['Offer']['metrics'])){	
						$metricas = json_decode($offer['Offer']['metrics'], true);
						$chaves = array_keys($metricas);						
						foreach($chaves as $chave){							
							if(empty($_SESSION['Carrinho']['Opcoes']['metricas'][$chave])){
								$this->Session->write('Carrinho.Opcoes.metricas.'.$chave, '');
							}								
						}											
					}
															
					//verifica se usuario ja tem CEP cadastrado - cria sessao e calcula frete
					if(!$this->Session->read('userData.User.zip_code')==false){						
						$frete = $this->calculaFrete($this->Session->read('userData.User.zip_code'));
						
						//pega dados do endereco					
						$this->Session->write('Carrinho.Endereco.endereco', $this->Session->read('userData.User.address'));
						$this->Session->write('Carrinho.Endereco.cidade', $this->Session->read('userData.User.city'));
						$this->Session->write('Carrinho.Endereco.cep', $this->Session->read('userData.User.zip_code'));
						$this->Session->write('Carrinho.Endereco.estado', $this->Session->read('userData.User.state'));
						$this->Session->write('Carrinho.Endereco.bairro', $this->Session->read('userData.User.district'));
						$this->Session->write('Carrinho.Endereco.numero', $this->Session->read('userData.User.number'));
						$this->Session->write('Carrinho.Endereco.descricao', 'Endereco cadastrado no PERFIL');																																				
					}	
					//vai no checkout para pegar id do pedido			
					$id_pedido = $this->Checkout(true);							
					$this->Session->write('Carrinho.Oferta.id_pedido', $id_pedido);											
				}														
			}			
			
			//calculando frete se for necessario
			if(!$this->Session->read('Carrinho.Endereco.cep')==false && $erro==false){													
				$frete = $this->calculaFrete($this->Session->read('Carrinho.Endereco.cep'));							
			}
			
			//verifica se precisa tratar cep 
			if(!$frete==false){
				$frete 			= explode("-", $frete);			
				$frete[0] 		= str_replace(',', '.', $frete[0]);
				
				$this->Session->write('Carrinho.Oferta.value_frete',  $frete[0]);
				$this->Session->write('Carrinho.Oferta.prazo_entrega', $frete[1]);
							
				$valorFinal = $this->Session->read('Carrinho.Oferta.value_total') + $this->Session->read('Carrinho.Oferta.value_frete');				
				
				$this->Session->write('Carrinho.Oferta.value_final', $valorFinal);
								
			}else{
				$this->Session->write('Carrinho.Oferta.value_frete',  '');
				$this->Session->write('Carrinho.Oferta.prazo_entrega', '');												
				$this->Session->write('Carrinho.Oferta.value_final', '');
				if($one==false)$erro = true;				
			}			
		}	
		
		//pegando enderecos do dito cujo
		$params = array('AditionalAddressesUser'=>array('conditions'=>array('user_id'=>$this->Session->read('userData.User.id'))));
		
		$enderecos = $this->Utility->urlRequestToGetData('users', 'all', $params);
		
		//verifica se empresa optou por frete fixo
		$params = array('CompanyPreference'=>array('fields'=>array('CompanyPreference.use_correios_api', 'CompanyPreference.delivery_time', 'CompanyPreference.shipping_value'),'conditions'=>array('CompanyPreference.company_id'=>$this->Session->read('Carrinho.Oferta.company_id'))));
		$companyPreference = $this->Utility->urlRequestToGetData('companies', 'first', $params);
		if(is_array($companyPreference)){			
			if($companyPreference['CompanyPreference']['use_correios_api']!=1){
				$this->Session->write('Carrinho.Oferta.value_frete', $companyPreference['CompanyPreference']['shipping_value']);
				$this->Session->write('Carrinho.Oferta.prazo_entrega', $companyPreference['CompanyPreference']['delivery_time']);
				$valueTotal = $this->Session->read('Carrinho.Oferta.value_total') + $this->Session->read('Carrinho.Oferta.value_frete');				
				$this->Session->write('Carrinho.Oferta.value_final', $valueTotal);				
			}
		}
		//pr($_SESSION['Carrinho']['Oferta']);exit;
		$this->set(compact('erro', 'token', 'enderecos'));		
		if(!empty($render))$this->render('Elements/ajax_detail_purchasing');
		
		
	}

	/**
	 * Executa funcao de checkout 
	 * retornando id da compra e token
	 * @return string
	 */
	private function Checkout($requisition=false, $token=false, $pagamento=false){		         		            
		if($requisition==true){
			//gerando numero do pedido			
			$arrayParams = array('Checkout'=>
									array(
										  'Checkout.user_id'=> $this->Session->read('userData.User.id'),
										  'Checkout.company_id'=> $this->Session->read('Carrinho.Oferta.company_id'),
										  'Checkout.payment_method_id'=>3,
										  'Checkout.offer_id'=> $this->Session->read('Carrinho.Oferta.id'),
									      'Checkout.payment_state_id'=>14,
										  'Checkout.unit_value'=> '0',
									      'Checkout.total_value'=> '0',
									      'Checkout.amount'=> '0',
									      'Checkout.shipping_value'=> '0',
									      'Checkout.shipping_type'=> 'CORREIOS',
									      'Checkout.delivery_time'=> '',
										  'Checkout.metrics'=> '',	
									      'Checkout.address'=> '',
									      'Checkout.city'=> '',
										  'Checkout.zip_code'=> '',
										  'Checkout.state'=> '',
										  'Checkout.district'=> '',
										  'Checkout.number'=> '',
										  'Checkout.complement'=> '',
										  'Checkout.date'=> date('Y-m/d'), 
										  'Checkout.transaction_moip_code'=> 0,
										  'Checkout.installment' => 0   	 												
									)
								);								
			$pagamento = $this->Utility->urlRequestToSaveData('payments', $arrayParams);					
			$dados = $pagamento['Checkout']['id'];			
		}else if($token==true){				
			
			//trabalhando metricas
			if($this->Session->check('Carrinho.Opcoes.metricas')){
				$metricas = json_encode($this->Session->read('Carrinho.Opcoes.metricas'));	
			}else{
				$metricas = '';
			}
				
						
			//verifica se usuario ja � assinante da empresa que fez a oferta										
			$params = array('CompaniesUser'=>array('conditions'=>array('CompaniesUser.company_id'=>$this->Session->read('Carrinho.Oferta.company_id'), 'CompaniesUser.user_id'=>$this->Session->read('userData.User.id'))));
			$countSignatures = $this->Utility->urlRequestToGetData('companies', 'count', $params);
			
			//verifica se usuario ja esta assinado a empresa
			if(!$countSignatures==true){					
				$params = array('CompaniesUser'=>array('company_id'=>$this->Session->read('Carrinho.Oferta.company_id'), 'user_id'=>$this->Session->read('userData.User.id'), 'status'=>'ACTIVE', 'last_status'=>'ACTIVE', 'date_register'=>date('Y/m/d')));
				//assina usuario a empresa que fez oferta publica									
				$signature = $this->Utility->urlRequestToSaveData('companies', $params);

				//pegando ofertas de empresa assinada e jogando para usuario de acordo com o perfil
				$offersCompany = $this->ofertas_perfil($this->Session->read('Carrinho.Oferta.company_id'), null, null, true);					
				foreach($offersCompany as $offerSignature){
					$offerId 		= $offerSignature['Offer']['id'];					
					$params 	= array('OffersUser'=>array('conditions'=>array('OffersUser.user_id'=>$this->Session->read('userData.User.id'), 'offer_id'=>$offerId)));
					$oferta     = $this->Utility->urlRequestToGetData('users', 'count', $params);
								
					if(!$oferta>0){
						$data 	= date('Y/m/d');
						$query		 	= "INSERT INTO offers_users values(NULL, '{$offerId}', '{$this->Session->read('userData.User.id')}', '{$data}', 'facebook - portal')";				
						$params		    = array('User' => array('query'=>$query));					
						$addUserOffer = $this->Utility->urlRequestToGetData('users', 'query', $params);
					}
				}
			}			
									
			//atualizando dados do pedido e gerando token
			$query = "UPDATE checkouts set 
								unit_value='{$this->Session->read('Carrinho.Oferta.value')}', 
								total_value='{$this->Session->read('Carrinho.Oferta.value_total')}',
								amount='{$this->Session->read('Carrinho.Opcoes.quantidade')}',
								shipping_value='{$this->Session->read('Carrinho.Oferta.value_frete')}',								
								delivery_time='{$this->Session->read('Carrinho.Oferta.prazo_entrega')}',
								metrics='{$metricas}',
								address='{$this->Session->read('Carrinho.Endereco.endereco')}',
								city='{$this->Session->read('Carrinho.Endereco.cidade')}',
								zip_code='{$this->Session->read('Carrinho.Endereco.cep')}',
								state='{$this->Session->read('Carrinho.Endereco.estado')}',
								district='{$this->Session->read('Carrinho.Endereco.bairro')}',
								number='{$this->Session->read('Carrinho.Endereco.numero')}' WHERE id = {$this->Session->read('Carrinho.Oferta.id_pedido')}";
							
            $params = array('User' => array('query' => $query));
            $checkout = $this->Utility->urlRequestToGetData('users', 'query', $params);
				
            //pegando email de contato de empresa
         	$company_id = $this->Session->read('Carrinho.Oferta.company_id');
         	$params = array('Company'=>array('fields'=>array('login_moip'),'conditions'=>array('Company.id'=>$company_id)));
         	$email_empresa = $this->Utility->urlRequestToGetData('companies', 'first', $params);
            
			//chamada do moip 
			$arrayParams = array('Payments'=>array(
								'email_empresa'=>$email_empresa['Company']['login_moip'],
								'porcentagem'=>'2.00',
								'parcelamento'=>$this->Session->read('Carrinho.Oferta.parcels'),
								'parcelamento_juros'=>$this->Session->read('Carrinho.Oferta.parcels_off_impost'),
								'key'=>'11PB4FPN68M1FE8MAPWUDIMEHFIGM8P6DMSBNXZZ', 
								'token'=>'JK75V6UGKYYUZR2ICVHJSSLD687UEJ9H', 
							   	'idUnique'=>$this->Session->read('Carrinho.Oferta.id_pedido'), 
							   	'reason'=>$this->Session->read('Carrinho.Oferta.title'),
							   	'value'=>$this->Session->read('Carrinho.Oferta.value_final'),										   
							   	'setPlayer'=>array(								 			
							   		'name'=>$this->Session->read('userData.User.name'),
									'email'=>$this->Session->read('userData.User.email'),
									'payerId'=>$this->Session->read('userData.User.id'),												 
							   		'billingAddress'=>array(
										'address'=>$this->Session->read('Carrinho.Endereco.endereco'), 
										'number'=>$this->Session->read('Carrinho.Endereco.numero'),
										'complement' => '',
							            'city' => $this->Session->read('Carrinho.Endereco.cidade'),
							            'neighborhood' => $this->Session->read('Carrinho.Endereco.bairro'),
							            'state' => $this->Session->read('Carrinho.Endereco.estado'),
							            'country' => 'BRA',
							            'zipCode' => $this->Session->read('Carrinho.Endereco.cep'),
							            'phone' => '(11)3929-2201'
										)
									)
								)
							);
			//pr($_SESSION['Carrinho']);exit;											
			$dados = $this->Utility->urlRequestToCheckout('payments', $arrayParams);
						
			if(!empty($dados['ID'])){
				//atualizando dados com numero do pedido gerado pelo MOIP
				$query = "UPDATE checkouts set 								
								transaction_moip_code='{$dados['ID']}' WHERE id = {$this->Session->read('Carrinho.Oferta.id_pedido')}";							
				$params = array('User' => array('query' => $query));				
				$checkout = $this->Utility->urlRequestToGetData('users', 'query', $params);			
			}										                        			
		}elseif($pagamento==true){						
			//atualizando dados com numero do pedido gerado pelo MOIP
			$query = "UPDATE checkouts set 								
								payment_method_id='{$this->request->data['tipo_pagamento']}', installment='{$this->request->data['vezes_pagamento']}'  WHERE id = {$this->Session->read('Carrinho.Oferta.id_pedido')}";									
			$params = array('User' => array('query' => $query));				
			$checkout = $this->Utility->urlRequestToGetData('users', 'query', $params);
		}		
		return $dados;
	}

	
	public function pagamento_mobile(){
		
	}
	
	public function pedidos_assinaturas($cancelar_solicitacoes=null){		
		$limit = 4;
		$update = true;
		
		$data  = date('Y-m-d');			
		$user_id = $this->Session->read('userData.User.id');
		
		if($cancelar_solicitacoes=='cancelar'){
			//retirando novos convites depois de visualizados									
			$query		      = "UPDATE companies_invitations_users SET status = 'INACTIVE' WHERE user_id = {$user_id} and status='WAIT'";				
			$params		      = array('User' => array('query'=>$query));							
			$convites_lidos   = $this->Utility->urlRequestToGetData('users', 'query', $params);		
		}
		
		
		
		//verifica se esta cancelando convite (antes de chamar query)
		if(!empty($this->request->data['excluir_convite'])){
			$render = true;
			$this->layout = '';		
			$id 		= $this->request->data['id'];
			
			//cancelando convite solicitado
			$params 	= array('CompaniesInvitationsUser'=>array('id'=>$id, 'status'=>'INACTIVE'));			
			$excluir 	= $this->Utility->urlRequestToSaveData('users', $params);			
		}
		
		//verifica se esta cancelando assinatura (antes de chamar query)
		if(!empty($this->request->data['assinar_empresa'])){
			$data = date('Y/m/d');
			$render = true;
			$this->layout = '';		
			$id 		 = $this->request->data['id'];
			$id_empresa  = $this->request->data['id_empresa'];
			
			//aceitando convite solicitado
			$params 	= array('CompaniesInvitationsUser'=>array('id'=>$id, 'status'=>'ACTIVE'));			
			$assinar 	= $this->Utility->urlRequestToSaveData('users', $params);

			//assinando usuario na empresa
			$params = array('CompaniesUser'=>array('company_id'=>$id_empresa, 'user_id'=>$user_id, 'status'=>'ACTIVE', 'last_status'=>'ACTIVE', 'date_register'=>date('Y/m/d')));													
			$signature = $this->Utility->urlRequestToSaveData('companies', $params);
			
			//pegando ofertas de acordo com o perfil
			$offersCompany = $this->ofertas_perfil($id_empresa, null, null, true);					
			foreach($offersCompany as $offerSignature){
				$offerId 		= $offerSignature['Offer']['id'];					
				$params 	= array('OffersUser'=>array('conditions'=>array('OffersUser.user_id'=>$this->Session->read('userData.User.id'), 'offer_id'=>$offerId)));
				$oferta     = $this->Utility->urlRequestToGetData('users', 'count', $params);
							
				if(!$oferta>0){	
					$query		 	= "INSERT INTO offers_users values(NULL, '{$offerId}', '{$this->Session->read('userData.User.id')}', '{$data}', 'facebook - portal')";				
					$params		    = array('User' => array('query'=>$query));					
					$addUserOffer = $this->Utility->urlRequestToGetData('users', 'query', $params);
				}
			}
		}
		
			
		$params   = array('CompaniesInvitationsUser'=>array('conditions'=>array('CompaniesInvitationsUser.user_id'=>$user_id, 'CompaniesInvitationsUser.status'=>'WAIT')));				
		$contador = $this->Utility->urlRequestToGetData('users', 'count', $params);
			
		//verifica se esta fazendo uma requisicao ajax
		if(!empty($this->request->data['limit'])){			
			$render = true;
			$this->layout = '';																				
			$limit = $_POST['limit'] + 4;
			if($limit > $contador){
				$limit = $contador;		
				$update = false;	 	
			} 				
		}
					
		
		$params = array('CompaniesInvitationsUser'=>array('conditions'=>array('CompaniesInvitationsUser.user_id'=>$user_id, 'CompaniesInvitationsUser.status'=>'WAIT'), 'order'=>array('CompaniesInvitationsUser.id'=>'DESC'), 'limit'=>$limit),'CompaniesInvitationsFilter', 'Companies');				
		$convitesAll = $this->Utility->urlRequestToGetData('users', 'all', $params);
		
		if(is_array($convitesAll)){
			foreach($convitesAll as $convite){
				$id_empresa = $convite['CompaniesInvitationsFilter']['company_id'];
				
				//pegando dados de empresa
				$params 	= array('Company'=>array('fields'=>array('Company.fancy_name', 'Company.description', 'phone', 'email'),'conditions'=>array('Company.id'=>$id_empresa)));							
				$empresa 	= $this->Utility->urlRequestToGetData('companies', 'first', $params);								
				$convite['CompaniesInvitationsFilter']['company']	    = $empresa['Company']['fancy_name'];
				$convite['CompaniesInvitationsFilter']['description']   = $empresa['Company']['description'];
				$convite['CompaniesInvitationsFilter']['phone']  		= $empresa['Company']['phone'];
				$convite['CompaniesInvitationsFilter']['email']  		= $empresa['Company']['email'];
				
				//quantidade de ofertas de empresa
				$params 		= array('Offer'=>array('conditions'=>array('Offer.company_id'=>$id_empresa, 'Offer.status'=>'ACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data)));
				$ofertas     	= $this->Utility->urlRequestToGetData('offers', 'count', $params);
				$convite['CompaniesInvitationsFilter']['ofertas'] = $ofertas;
				
				//pegando ofertas de acordo com perfil
				$ofertas_perfil = $this->ofertas_perfil($id_empresa);
				$quantidade_perfil = count($ofertas_perfil);
				
				$convite['CompaniesInvitationsFilter']['ofertas_perfil'] = $quantidade_perfil;
									
				$convites[] = $convite;							
			}
		}else{
			$convites = 'VOCE NAO TEM CONVITES NO MOMENTO';
		}		
				
		$this->set(compact('convites', 'update', 'limit', 'contador'));
		if(!empty($render)){
			$this->render('Elements/ajax_pedidos_assinaturas');
		}else{
			
			//retirando novos convites depois de visualizados									
			$query		      = "UPDATE companies_invitations_users SET preview = 'ACTIVE' WHERE user_id = {$user_id}";				
			$params		      = array('User' => array('query'=>$query));							
			$convites_lidos   = $this->Utility->urlRequestToGetData('users', 'query', $params);
		}				
	}
	
	
	
	public function wishlist($page=null, $id=null){
		//verifica se usuario vai visualizar ofertas para desejo realizado
		if($page=='offers'){
					
			//pegando todas as ofertas direcionadas para desejo realizado			
			$params   = array('UsersWishlistCompany'=>array('fields'=>array('UsersWishlistCompany.offer_id'), 'conditions'=>array('UsersWishlistCompany.wishlist_id'=>$id, 'UsersWishlistCompany.offer_id > '=>0)));			
			$ofertas  = $this->Utility->urlRequestToGetData('companies', 'all', $params);
			
			foreach($ofertas as $offer){				
				$offers[] = $offer['UsersWishlistCompany']['offer_id'];
			}								
			$this->Session->write('ofertasIds', $offers);			
			
			$this->redirect(array('controller' => 'users', 'action' => 'home', 'plugin' => 'users'));
			
		}else if($page=='cadastro'){						
			//verifica se � feito requisicao ajax para carregar subcategorias
			if($this->request->is('ajax')) {
					if(!empty($this->request->data['categoria'])){
						$render = true;
						$this->layout = '';		
						$arrayParams = array(
							'CompaniesSubCategory'=> array('conditions'=>array('CompaniesSubCategory.category_id'=>$this->request->data['categoria']),array('CompaniesCategory'))																			
						);	
											
						$sub_categorias = $this->Utility->urlRequestToGetData('companies', 'all', $arrayParams);																						
					}
					$this->set(compact('sub_categorias'));
					if(!empty($render))$this->render('Elements/ajax_categorias_wishlist');
				}else{
					$arrayParams = array(
						'CompaniesCategory'=> array(							
						)						
					);
					$categorias      = $this->Utility->urlRequestToGetData('companies', 'all', $arrayParams);						
					
					if($this->request->is('post')){
						$cad_desejo = $this->Utility->urlRequestWishlist('users', 'all', $this->request['data']);
						if($cad_desejo['status']=='SAVE_OK'){
							$this->redirect(array('controller' => 'users', 'action' => 'wishlist', 'plugin' => 'users'));	
						}else{
							$mensagem = "Ocorreu um erro no cadastro. Tente novamente";
						}																
					}
					$this->set(compact('categorias', 'mensagem'));
					$this->render('cad_wishlist');
				}			
		}else{			
			$limit = 4;
			$update = true;
		
			$data  = date('Y-m-d');			
			$user_id = $this->Session->read('userData.User.id');				
		
		
			$params   = array('UsersWishlist'=>array('conditions'=>array('UsersWishlist.user_id'=>$user_id, 'UsersWishlist.status'=>'ACTIVE')));				
			$contador = $this->Utility->urlRequestToGetData('users', 'count', $params);
			
		
			//verifica se esta cancelando desejo (antes de chamar query)
			if(!empty($this->request->data['excluir_desejo'])){
				$render = true;
				$this->layout = '';		
				$id = $this->request->data['id'];
				
				//cancelando desejo solicitado
				$params 	= array('UsersWishlist'=>array('id'=>$id, 'status'=>'INACTIVE'));			
				$excluir 	= $this->Utility->urlRequestToSaveData('users', $params);			
			}
		
		
			//verifica se esta fazendo uma requisicao ajax
			if(!empty($this->request->data['limit'])){			
				$render = true;
				$this->layout = '';																				
				$limit = $_POST['limit'] + 4;
				if($limit > $contador){
					$limit = $contador;		
					$update = false;	 	
				} 				
			}
		
			$params = array('UsersWishlist'=>array('conditions'=>array('UsersWishlist.user_id'=>$user_id, 'UsersWishlist.status'=>'ACTIVE'), 'order'=>array('UsersWishlist.id'=>'DESC'), 'limit'=>$limit),'User', 'CompaniesCategory', 'CompaniesSubCategory', 'UsersWishlistCompany');				
			$lista_desejos = $this->Utility->urlRequestToGetData('users', 'all', $params);	
			
			$this->set(compact('lista_desejos', 'update', 'limit', 'contador'));
			
			if(!empty($render))$this->render('Elements/ajax_desejos');
		}		
	}
	
	
	
	public function sucesso($page=null){
		$mensagem = "PAGINA INVALIDA";
		$link = "HOME";
		$descLink = array('controller'=>'users', 'plugin'=>'users', 'action'=>'home');
		
		switch($page){
			case "compras":
				$mensagem  = "Compra realizada! <br> Maiores detalhes em";
				$link = "Minhas Compras";
				$descLink = array('controller'=>'users', 'plugin'=>'users', 'action'=>'minhasCompras');
			break;
		}
				
		$this->set(compact('mensagem', 'link', 'descLink'));
	}
	
	
	public function dados_cadastrais(){
		
		
		if($this->request->is('ajax')) {
			if(!empty($this->request->data['cep_buscar'])){
				$cURL = curl_init("http://cep.correiocontrol.com.br/{$this->request->data['cep_buscar']}.json");						
				curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);						
				$resultado = curl_exec($cURL);						
				curl_close($cURL);											
				echo $resultado;exit;		
			}
		}		
		else if($this->request->is('post')){						
			$this->request->data['User']['birthday'] = date('Y-m-d', strtotime($this->request->data['User']['birthday']));
			$arrayParams = array('User'=>$this->request->data['User']);								
			$editar  	 = $this->Utility->urlRequestToSaveData('users', $arrayParams);			
			$this->Session->write('userData.User', $editar['data']['User']);						
		}		
		
	}
	
	
	public function new_password(){
		$this->layout = 'login';
		if($this->request->data){			
			$arrayParams = array(
			  'User'=>array(
			   'fields' => array('User.id', 'User.name', 'User.email'),
			   'conditions' => array('User.email' => $this->request->data['User']['email'])
			  )
			 );
			 
			$nova_senha = $this->Utility->urlRequestPasswordRecovery('users', 'first', $arrayParams);
			
			if(!$nova_senha){
				$mensagem = 'Email informado nao foi encontrado no trueone';
			}else{
				$mensagem = 'Senha alterada com sucesso, verifique sua caixa de entrada';
			}			
			$this->set(compact('mensagem'));
		}				
	}
	
	/**
	 * Executa calculo de frete
	 * chamando funcao que trabalha API dos correios
	 * @return string
	 */
	private function calculaFrete($cepUser){
		$total 	= $this->Session->read('Carrinho.Opcoes.quantidade') * $this->Session->read('Carrinho.Oferta.value');											
		$totalPeso  = $this->Session->read('Carrinho.Opcoes.quantidade') * $this->Session->read('Carrinho.Oferta.weight');
				
		$this->Session->write('Carrinho.Oferta.value_total', $total);
		$this->Session->write('Carrinho.Oferta.weight_total', $totalPeso);							
			
		//pegando CEP de origem(empresa) e fazendo calculo de frete
		$params = array('Company'=>array('fields'=>array('Company.zip_code'),'conditions'=>array('id'=>$this->Session->read('Carrinho.Oferta.company_id'))));
		$cep 	= $this->Utility->urlRequestToGetData('companies', 'first', $params);
		$cep 	= $cep['Company']['zip_code'];	
		$frete  = $this->Utility->calcFrete('41106', $cep, $cepUser, $this->Session->read('Carrinho.Oferta.weight_total'));
		if($frete==false){
			return false;
		}else{
			return $frete;	
		}		
	}
	
	
	/**
	 * Verifica se perfil de usuario esta completo	 
	 * @return bool
	 */
	private function verificaPerfil(){		
		if($this->Session->check('userData.User')){
			//verificando se perfil esta completo ou nao			
			if($_SESSION['userData']['User']['gender']==''){
				return true;
			}else if($_SESSION['userData']['User']['birthday']==''){
				return true;
			}else if($_SESSION['userData']['User']['address']==''){
				return true;
			}else if($_SESSION['userData']['User']['city']==''){
				return true;
			}else if($_SESSION['userData']['User']['zip_code']==''){
				return true;
			}else if($_SESSION['userData']['User']['state']==''){
				return true;
			}else if($_SESSION['userData']['User']['district']==''){
				return true;
			}else if($_SESSION['userData']['User']['number']==''){
				return true;
			}else if($_SESSION['userData']['User']['photo']=='http://acclabs.accential.com.br/uploads/img-users/male.jpg'){
				return true;
			}else if($_SESSION['userData']['User']['photo']=='http://acclabs.accential.com.br/uploads/img-users/female.jpg'){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * Retorna ofertas personalizadas de acordo com perfil de usuario - variaveis - facebook	 
	 * @return bool
	 */
	private function ofertas_perfil($id_empresa=null, $lista_ofertas=false, $limit=null, $assinatura_oferta_perfil=null){
		$data = date('Y/m/d');
		$ofertas_personalizadas = array();
		
		//quantidade de ofertas para perfil de usuario logado				
		if(!$this->Session->check('userData.FacebookProfile')){
			$political = '';
			$relationship_status = '';
			$religion = '';
			$location = '';
		}else{
			$political 			 	= $this->Session->read('userData.FacebookProfile.political');
			$relationship_status 	= $this->Session->read('userData.FacebookProfile.relationship_status');
			$religion 				= $this->Session->read('userData.FacebookProfile.religion');
			$location 				= $this->Session->read('userData.FacebookProfile.location');
		}							
		
		if($assinatura_oferta_perfil==true){
			$params = array('Offer'=>array('conditions'=>array('Offer.company_id'=>$id_empresa, 'Offer.status'=>'ACTIVE', 'Offer.public'=>'INACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data)), 'OffersFilter');	
		}else{
			$params = array('Offer'=>array('conditions'=>array('Offer.company_id'=>$id_empresa, 'Offer.status'=>'ACTIVE', 'Offer.public'=>'INACTIVE', 'Offer.begins_at <= '=> $data, 'Offer.ends_at >= '=>$data), 'order'=>'Offer.id DESC'), 'OffersFilter');
		}
																								
		$ofertas_perfil = $this->Utility->urlRequestToGetData('offers', 'all', $params);
						
		foreach($ofertas_perfil as $oferta_perfil){			
			if(is_array($oferta_perfil['OffersFilter'][0])){
				foreach($this->offerFilters as $key => $value) {
					if(!empty($oferta_perfil['OffersFilter'][0][$key])){																
						$array = explode(',', $oferta_perfil['OffersFilter'][0][$key]);
						if(!in_array($this->Session->read('userData.FacebookProfile.'.$key), $array)){
							unset($oferta_perfil);
						}						
					}	
				}														
			}else{
				unset($oferta_perfil);
			}							
			if(is_array($oferta_perfil)){
				$ofertas_personalizadas[] = $oferta_perfil;
			}											
		}
		
		if($limit > 0 && count($ofertas_personalizadas) >= $limit){			
			$i = 0;
			for($i=0; $limit > $i; $i++){
				$ofertas[$i] = $ofertas_personalizadas[$i];		
			}	
		}else{
			$ofertas = $ofertas_personalizadas;
		}				
		return $ofertas;
	}
	
	
	// Define uma fun��o que poder� ser usada para validar e-mails usando regexp
	public function validaEmail($email) {
		$conta = "^[a-zA-Z0-9\._-]+@";
		$domino = "[a-zA-Z0-9\._-]+.";
		$extensao = "([a-zA-Z]{2,4})$";
		
		$pattern = $conta.$domino.$extensao;
		
		if (ereg($pattern, $email))
		return true;
		else
		return false;
	}	
}
?>